using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace EdnaStartCS
{
	internal static class Program
	{
		public static string preferences;

		public static string sJVMStartOptions = "-Xmx512m -Xms512m -Xincgc ";

		public static bool isGerman;

		public static bool isEnglish;

		public static bool isFullScreen;

		public static bool isMusicOn;

		public static bool isSpeechOn;

		public static bool isSubtitleOn;

		public static bool isCommentOn;

		public static bool ignoreWindowWarning;

		public static bool isHardwareAccelerated;

		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Program.readPreferences();
			Application.Run(new EdnaForm());
		}

		private static void readPreferences()
		{
			FileStream expr_15 = new FileStream(Application.StartupPath + Path.DirectorySeparatorChar + "ednaPreferen.ces", FileMode.Open);
			StreamReader expr_1B = new StreamReader(expr_15);
			Program.preferences = expr_1B.ReadToEnd();
			expr_1B.Close();
			expr_15.Close();
			string arg_A6_0 = Program.readEntry("language\"", 17, 2);
			string text = Program.readEntry("fullscreen\"", 19, 5);
			Program.readEntry("bufferStrategy\"", 23, 5);
			string text2 = Program.readEntry("music\"", 14, 5);
			string text3 = Program.readEntry("sound\"", 14, 5);
			string text4 = Program.readEntry("text\"", 13, 5);
			string text5 = Program.readEntry("comment\"", 16, 5);
			string text6 = Program.readEntry("accelerated\"", 20, 5);
			if (arg_A6_0.Equals("de"))
			{
				Program.isGerman = true;
			}
			else
			{
				Program.isEnglish = true;
			}
			try
			{
				Program.isFullScreen = bool.Parse(text);
				Program.isMusicOn = bool.Parse(text2);
				Program.isSpeechOn = bool.Parse(text3);
				Program.isSubtitleOn = bool.Parse(text4);
				Program.isCommentOn = bool.Parse(text5);
				Program.isHardwareAccelerated = bool.Parse(text6);
				Program.ignoreWindowWarning = !Program.isFullScreen;
			}
			catch (FormatException arg_10F_0)
			{
				MessageBox.Show(arg_10F_0.Message);
			}
		}

		public static void writePreferences()
		{
			string arg_1D_0 = "language\"";
			string replaceWith = Program.isGerman ? "de" : "en";
			Program.writeEntry(arg_1D_0, 17, 2, replaceWith);
			string arg_3F_0 = "fullscreen\"";
			replaceWith = (Program.isFullScreen ? "true" : "false");
			Program.writeEntry(arg_3F_0, 19, 5, replaceWith);
			string arg_61_0 = "music\"";
			replaceWith = (Program.isMusicOn ? "true" : "false");
			Program.writeEntry(arg_61_0, 14, 5, replaceWith);
			string arg_83_0 = "sound\"";
			replaceWith = (Program.isSpeechOn ? "true" : "false");
			Program.writeEntry(arg_83_0, 14, 5, replaceWith);
			string arg_A5_0 = "text\"";
			replaceWith = (Program.isSubtitleOn ? "true" : "false");
			Program.writeEntry(arg_A5_0, 13, 5, replaceWith);
			string arg_C7_0 = "comment\"";
			replaceWith = (Program.isCommentOn ? "true" : "false");
			Program.writeEntry(arg_C7_0, 16, 5, replaceWith);
			string arg_E9_0 = "accelerated\"";
			replaceWith = (Program.isHardwareAccelerated ? "true" : "false");
			Program.writeEntry(arg_E9_0, 20, 5, replaceWith);
			FileStream expr_103 = new FileStream(Application.StartupPath + Path.DirectorySeparatorChar + "ednaPreferen.ces", FileMode.OpenOrCreate);
			StreamWriter expr_109 = new StreamWriter(expr_103);
			expr_109.Write(Program.preferences);
			expr_109.Close();
			expr_103.Close();
		}

		public static void startEdna()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (Program.isCommentOn)
			{
				stringBuilder.Append("-commentOff ");
			}
			if (Program.isHardwareAccelerated)
			{
				stringBuilder.Append("-accelerated");
			}
			Process.Start("EdnaWrapper.exe", stringBuilder.ToString());
		}

		private static string readEntry(string splitString, int width, int length)
		{
			string text = "";
			int num = Program.preferences.IndexOf(splitString);
			try
			{
				text = Program.preferences.Substring(num + width, length);
			}
			catch (ArgumentOutOfRangeException)
			{
			}
			if (text.StartsWith("t"))
			{
				text = text.Substring(0, 4);
			}
			return text;
		}

		private static void writeEntry(string lookFor, int startAt, int width, string replaceWith)
		{
			int num = Program.preferences.IndexOf(lookFor) + startAt;
			int num2 = width;
			string arg_67_0 = Program.preferences.Substring(0, num);
			if (Program.preferences.Substring(num, num2).StartsWith("t"))
			{
				Program.preferences.Substring(num, 4);
				num2 = 4;
			}
			int num3 = num + num2;
			string text = Program.preferences.Substring(num3, Program.preferences.Length - num3);
			Program.preferences = arg_67_0 + replaceWith + text;
		}
	}
}
