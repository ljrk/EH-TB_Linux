# Edna & Harvey: The Breakout -- Linux Patches

## Patch

### SteamJavaWrapper

 1. Download the latest Steamworks SDK from
    https://partner.steamgames.com/downloads/list and extract it to Steam, s.t.
    `Steam/sdk/public/steam/` holds the API headers (.h files)
 2. Install Swig and the Java SDK
 2. Run `make` from the Steam subfolder, you may need to specify the location of
    the JAVA_SDK, eg.: `make JAVA_SDK=/usr/lib/jvm/default` (should hold an
    `include` directory)
 3. Copy the `libSteamJavaWrapper.so` and `libSteamJavaWrapper64.so` into the
    game directory.
 4. Also copy `Steam/sdk/redistributable_bin/linux32/libsteam_api.so` as is and
    `Steam/sdk/redistributable_bin/linux64/libsteam_api.so` as
    `libsteam_api64.so` into the game directory.
 5. Copy `steam_appid.txt` into the game directory.

### LWJGL and OpenAL

 1. Download LWJGL from http://legacy.lwjgl.org/download.php.html
 2. Copy `native/linux/lwjgl[64].so` and
   `native/linux/libopenal[64].so`
    as `liblwjgl[64].so` resp. `libOpenAL[64].so`
    into the game's root directory.
 3. Replace `lib/lwjgl.jar` by the one downloaded `jar/liblwjgl.jar`

## Launching

### Using the script

Eventually modify the well-documented script from the "Launcher" subfolder and
then just run it:

    $ ./Edna.sh

### Launching manually

Run `java -cp lib/*.jar:data/*.jar -jar Edna.jar` with the options you want, the
script can be used as an example (note: the glob '`*`' might not expand,
so you may need to list all the JAR files manually).

### Using the Launchers

There's the decompiled and patched sourcecode for the launchers with a
ready-made solution in the "Launcher" subdirectory.
You will need mono to compile it using, `msbuild`.
Then copy the results (.exe files) from both folders' bin/Debug/ subfolder into
the game directory, overwriting the originals

## Adding shortcuts

Edit the .desktop files to point to the script or executables and install them
using `xdg-desktop-menu install --novendor dot-desktop-file.desktop`.
